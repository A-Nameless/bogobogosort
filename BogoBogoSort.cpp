#include <iostream>
#include <cstdlib>
#include <ctime>
#include <bits/stdc++.h>
#include <chrono>

using namespace std;

void addSeperator(string& string, char seperator = ',') {
    int index = string.length() - 3;

    while (index > 0) {
        string.insert(index, 1, seperator);
        index -= 3;
    }
}

void printArray(int* array, int size) {
    cout << "[";
    for (int i = 0; i < size; i++) {
        cout << array[i];
        if (size - 1 != i) {
            cout << ", ";
        }
    }
    cout << "]" << endl;
}

void shuffleArray(int* array, int size) {
    shuffle(array, array + size, default_random_engine(rand()));
}

void bogoBogoSort(int* array, int size) {
    if (size == 1) { return; }

    while (true)
    {
        bogoBogoSort(array, size - 1);

        if (array[size-2] <= array[size-1]) {
            return;
        }
        else {
            shuffleArray(array, size);
        }
    }
}

int main() {
    cout << "Enter a number of elements: ";
    int n;
    cin >> n;
    int numbers[n];

    srand(time(nullptr));

    for (int i = 0; i < n; ++i) {
        numbers[i] = rand() % 1000;
    }

    printArray(numbers, n);

    auto begin = chrono::high_resolution_clock::now();

    bogoBogoSort(numbers, n);

    auto end = chrono::high_resolution_clock::now();

    printArray(numbers, n);

    string timeTaken = to_string(chrono::duration_cast<chrono::nanoseconds>(end-begin).count());
    addSeperator(timeTaken);
    cout << "Took: " << timeTaken << " ns" << endl;
}